import React, {useState,useEffect} from 'react'
import { Button,Carousel,Card,Avatar, Tooltip, Spin } from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import ModalContent from '../../Components/Modalcontent/modalcontent'
import {db} from '../../Firebase/config'
import './home.css'
import Bcg_Photo from "../../Assets/bcg_photo.svg" 

const { Meta } = Card;

const Home = () => {
    const [isVisible,setVisible] = useState(false)
    const [posts,setposts]=useState([])
    const [users, setusers] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
    const showModal = () => {
      setVisible(true);
    };
      
    useEffect(() => {
      fetchUsers();
      fetchPosts()
    }, [])

    const fetchUsers = async () => {
      const response=db.collection('users');
      const data=await response.get();
      var usrData=[]
      data.docs.forEach(item=>{
        usrData.push(item.data())
        })
        setusers(usrData)
        setIsLoading(false)
    }

    const fetchPosts = async () =>{
      const response=db.collection('posts');
       const observer = response.onSnapshot(data => {
        var pstData=[]
        data.docs.forEach(item=>{
          pstData.push(item.data())
         })
         setposts(pstData.sort((a,b)=>b.post_id - a.post_id))
        // ...
      }, err => {
        console.log(`Encountered error: ${err}`);
      });
      
       
    }
    const renderPosts = () => {
      return posts.map((pst,i)=>{
       var postUsr=users.filter(a=>a.id==pst.user_id)[0]
        return  <Card
        hoverable
        cover={<Carousel >{pst.images.map((el,i)=>{
          return <img className='img-carousel' src={`${el}`}/>
        })}</Carousel>}
      >
        <Meta avatar={
              <Avatar src={postUsr.usrpic} />
            } title={`${postUsr.name} ${postUsr.surname}`} description={`${pst.text}`} />
      </Card>
        
      })
    }
 
    return (
        <div className='home-wrapper'>
        <img src={Bcg_Photo} className='bcg-photo'></img>
          <Spin tip="Almost there..." size='large'  spinning={isLoading}>
        <div className='posts'>  
          {renderPosts()}
        </div> 
        </Spin>
        <Tooltip title="Create post">
            <Button type="primary" shape="circle" icon={<PlusOutlined />} size='large' onClick={showModal}>
            </Button>
        </Tooltip>
        <ModalContent isVisible={isVisible} setVisible={setVisible} />
        </div>
    )
}

export default Home;